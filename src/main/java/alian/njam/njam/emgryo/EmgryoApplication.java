package alian.njam.njam.emgryo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmgryoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmgryoApplication.class, args);
	}
}
