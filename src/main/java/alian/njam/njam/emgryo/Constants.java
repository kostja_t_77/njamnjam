package alian.njam.njam.emgryo;

public interface Constants {

	public static final String URL_BASE = "api";
	public static final String URL_ALIAN = "alien";
	public static final String URL_HUMAN = "human";
	public static final String URL_MATCH = "match";
	
}