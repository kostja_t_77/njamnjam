package alian.njam.njam.emgryo.controller;

import static alian.njam.njam.emgryo.Constants.URL_BASE;
import static alian.njam.njam.emgryo.Constants.URL_ALIAN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import alian.njam.njam.emgryo.model.Alien;
import alian.njam.njam.emgryo.service.AlianService;

@RestController
@RequestMapping(URL_BASE + "/" + URL_ALIAN)
public class AlianController {
	
	@Autowired
	private AlianService alianService;
	
	@GetMapping
	public List<Alien> getList(){
		return alianService.generateAliens(1);
	}

}
