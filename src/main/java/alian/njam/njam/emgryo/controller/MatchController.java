package alian.njam.njam.emgryo.controller;

import static alian.njam.njam.emgryo.Constants.URL_BASE;
import static alian.njam.njam.emgryo.Constants.URL_MATCH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import alian.njam.njam.emgryo.model.Contact;
import alian.njam.njam.emgryo.service.MatchService;

@RestController
@RequestMapping(URL_BASE + "/" + URL_MATCH)
public class MatchController {

	@Autowired
	private MatchService matchService;

	@PostMapping
	public Contact getAccountList(@RequestBody Contact contact) {
		return matchService.njamNjam(contact);
	}
	
}