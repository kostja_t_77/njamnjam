package alian.njam.njam.emgryo.controller;

import static alian.njam.njam.emgryo.Constants.URL_BASE;
import static alian.njam.njam.emgryo.Constants.URL_HUMAN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import alian.njam.njam.emgryo.model.Human;
import alian.njam.njam.emgryo.service.HumanService;

@RestController
@RequestMapping(URL_BASE + "/" + URL_HUMAN)
public class HumanController {
	
	@Autowired
	private HumanService humanService;
	
	@GetMapping
	public List<Human> getList(){
		return humanService.generateHumans(3);
	}

}
