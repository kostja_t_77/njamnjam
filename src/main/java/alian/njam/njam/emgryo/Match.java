package alian.njam.njam.emgryo;

public enum Match {
	
	GREAT_SUCCESS("new viable and suiphurous creature has left the nest"),
	BIG_SUCCESS("new viable and strong creature has left the nest"),
	SUCCESS("new alien has left the nest"),
	FAIL("gestating aborted");
	
    private final String message;
    
    Match(String message) {
        this.message = message;
    }

	public String getMessage() {
		return message;
	}

}