package alian.njam.njam.emgryo;

import java.util.UUID;

public class Tools {
	
	public static final Integer DNA_RANGE = 9;
	public static final Integer DNA_MIN_NUMBER = 1;
	public static final String HUMAN_NAME_PREFIX = "ANNA K";
	public static final String HUMAN_NAME_SUFIXS = "OVA";
	public static final String ALIEN_NAME_PREFIX = "Zloi Prishelits ";
	public static final String ALIEN_NAME_SUFIXS = " !!!";
	
	public static Integer generateDNA(){
		return DNA_MIN_NUMBER + (int)(Math.random() * DNA_RANGE); 
	}
	
	public static String generateHumanName(){
		return HUMAN_NAME_PREFIX + UUID.randomUUID().toString() + HUMAN_NAME_SUFIXS;
	}

	public static String generateAlienName(){
		return ALIEN_NAME_PREFIX + UUID.randomUUID().toString() + ALIEN_NAME_SUFIXS;
	}
	
    public static Match resolveTriangle(int ... sides) {

        int firstSide = sides[0];
        int secondSide = sides[1];
        int thirdSide = sides[2];

        if (firstSide + secondSide <= thirdSide || secondSide + thirdSide <= firstSide || firstSide + thirdSide <= secondSide) {
            return Match.FAIL; //invalid
        } else if (firstSide <= 0 || secondSide <= 0 || thirdSide <= 0) {
            return Match.FAIL; //invalid
        } else if (firstSide == secondSide && secondSide == thirdSide) {
            return Match.GREAT_SUCCESS; //equilateral
        } else if (firstSide == secondSide || secondSide == thirdSide || thirdSide == firstSide) {
            return Match.BIG_SUCCESS; //isosceles
        } else {
            return Match.SUCCESS; //scalene
        }
    }
	
}