package alian.njam.njam.emgryo.service;

import static  alian.njam.njam.emgryo.Tools.resolveTriangle;

import org.springframework.stereotype.Service;

import alian.njam.njam.emgryo.model.Contact;

@Service
public class MatchService {

	public Contact njamNjam(Contact contact){
		Integer one = contact.getAlien().getDna();
		Integer two = contact.getHuman().getDna()[0];
		Integer three = contact.getHuman().getDna()[1];
		contact.setResult(resolveTriangle(one, two, three));
		return contact;
	}
	
}