package alian.njam.njam.emgryo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import alian.njam.njam.emgryo.model.Human;

import static alian.njam.njam.emgryo.Tools.generateDNA;
import static alian.njam.njam.emgryo.Tools.generateHumanName;;

@Service
public class HumanService {
	
	public List<Human> generateHumans(int number){
		List<Human> ret = new ArrayList<>();
		for (int i = 0; i < number; i++) {
			ret.add(generateHuman());
		}
		return ret;
	}

	public Human generateHuman(){
		Human ret = new Human();
		Integer[] array = new Integer[2];
		array[0] = generateDNA();
		array[1] = generateDNA();
		ret.setDna(array);
		ret.setName(generateHumanName());
		ret.setUrl("");//TODO
		return ret;
	}

}
