package alian.njam.njam.emgryo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import alian.njam.njam.emgryo.model.Alien;

import static alian.njam.njam.emgryo.Tools.generateDNA;
import static alian.njam.njam.emgryo.Tools.generateAlienName;

@Service
public class AlianService {
	
	public List<Alien> generateAliens(int number){
		List<Alien> ret = new ArrayList<>();
		if(number > 0){
			ret.add(generateAlien());
		}
		return ret;
	}

	public Alien generateAlien(){
		Alien ret = new Alien();
		ret.setDna(generateDNA());
		ret.setName(generateAlienName());
		ret.setUrl("");//TODO
		return ret;
	}
	
}