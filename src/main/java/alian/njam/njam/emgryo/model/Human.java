package alian.njam.njam.emgryo.model;

public class Human {

	private Integer[] dna;
	private String name;
	private String url;
	
	public Integer[] getDna() {
		return dna;
	}
	public void setDna(Integer[] dna) {
		this.dna = dna;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}