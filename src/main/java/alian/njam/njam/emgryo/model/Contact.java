package alian.njam.njam.emgryo.model;

import alian.njam.njam.emgryo.Match;

public class Contact {
	
	private Alien alien;
	private Human human;
	private Match result;
	
	public Alien getAlien() {
		return alien;
	}
	public void setAlien(Alien alien) {
		this.alien = alien;
	}
	public Human getHuman() {
		return human;
	}
	public void setHuman(Human human) {
		this.human = human;
	}
	public Match getResult() {
		return result;
	}
	public void setResult(Match result) {
		this.result = result;
	}
	
}