package alian.njam.njam.emgryo;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import alian.njam.njam.emgryo.model.Alien;
import alian.njam.njam.emgryo.model.Human;

import static alian.njam.njam.emgryo.Constants.URL_BASE;
import static alian.njam.njam.emgryo.Constants.URL_ALIAN;
import static alian.njam.njam.emgryo.Constants.URL_HUMAN;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmgryoApplicationTests {
	
	@Autowired
	private TestRestTemplate restTemplate; 

	@Test
	public void testAliens() {	
		String url = "/" + URL_BASE + "/" + URL_ALIAN + "/";
		ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
		
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		List<Alien> list = responseEntity.getBody();
		
		assertNotNull(list);
		assertEquals(list.size(), 1);
	}
	
	@Test
	public void testHumans() {	
		String url = "/" + URL_BASE + "/" + URL_HUMAN + "/";
		ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
		
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		List<Human> list = responseEntity.getBody();
		
		assertNotNull(list);
		assertEquals(list.size(), 3);
	}

}